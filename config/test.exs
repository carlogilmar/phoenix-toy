use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :toy_phoenix, ToyPhoenixWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :toy_phoenix, ToyPhoenix.Repo,
  username: "postgres",
  password: "postgres",
  database: "toy_phoenix_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
