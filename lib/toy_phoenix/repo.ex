defmodule ToyPhoenix.Repo do
  use Ecto.Repo,
    otp_app: :toy_phoenix,
    adapter: Ecto.Adapters.Postgres
end
