defmodule ToyPhoenix.Repo.Migrations.AddField do
  use Ecto.Migration

  def change do
    create table(:jobs, primary_key: false) do
      add :id, :serial, primary_key: true
      add :position, :string
      timestamps()
    end
  end
end
